const btnTheme = document.querySelector('button')

if (localStorage.getItem('style') == 'silver') {
    document.body.classList.toggle('silver')
}

btnTheme.onclick = function(){
    document.body.classList.toggle('silver')
    if(document.body.getAttribute('class') == 'silver') {
        localStorage.setItem('style', 'silver')
        } else {
        localStorage.setItem('style', '')
    }
}






